from django.apps import apps
from django.contrib import admin

for model in apps.get_app_config("django_mdat_security_txt").models.values():
    admin.site.register(model)
