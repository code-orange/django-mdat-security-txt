from django.http import HttpResponse

from django.conf import settings

from django_mdat_customer.django_mdat_customer.models import MdatCustomers
from django_mdat_security_txt.django_mdat_security_txt.models import MdatSecurityTxt


def security_txt(request):
    customer = MdatCustomers.objects.get(id=settings.MDAT_ROOT_CUSTOMER_ID)
    security_txt_attributes = MdatSecurityTxt.objects.filter(
        customer=customer
    ).order_by("element__key", "value")

    content = "# " + customer.name + "\n\n"

    for security_txt_attribute in security_txt_attributes:
        content += "# " + security_txt_attribute.element.comment + "\n"
        content += (
            security_txt_attribute.element.key
            + ": "
            + security_txt_attribute.value
            + "\n\n"
        )

    return HttpResponse(content, content_type="text/plain")
