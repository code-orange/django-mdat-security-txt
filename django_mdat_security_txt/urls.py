from django.urls import path

from . import views

urlpatterns = [
    path("", views.security_txt, name="security_txt"),
]
