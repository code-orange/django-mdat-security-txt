from django.db import models

from django_mdat_customer.django_mdat_customer.models import MdatCustomers


class MdatSecurityTxtElement(models.Model):
    key = models.CharField(max_length=250, unique=True)
    comment = models.CharField(max_length=250)

    def __str__(self):
        return self.key

    class Meta:
        db_table = "mdat_security_txt_element"


class MdatSecurityTxt(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    element = models.ForeignKey(MdatSecurityTxtElement, models.DO_NOTHING)
    value = models.CharField(max_length=250)

    def __str__(self):
        return self.customer.name + " - " + self.element.key + " - " + self.value

    class Meta:
        db_table = "mdat_security_txt"
